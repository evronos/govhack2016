from __future__ import print_function
from flask import Flask
import requests
import json
import xmltodict
from flask_cache import Cache
from trademarklearner import trademarkClassifier
from flask import request as flask_request


from flask_cors import CORS
app = Flask(__name__)
CORS(app)
app.config['CACHE_TYPE'] = 'simple'
cache = Cache(app)

f = open("private/abr_authentication_token.txt")
authenticationGuid = f.read().strip()

# Create trademark classifier object
tc = trademarkClassifier.TrademarkClassifier()

def get_all_values(dictionary):
    all_values=[]
    for key, value in dictionary.iteritems():
        all_values.append(key)

        if isinstance(value, dict):
            results = get_all_values(value)
            for result in results:
                all_values.append(result)

        elif isinstance(value, list):
            for item in value:
                if isinstance(item, dict):
                    more_results = get_all_values(item)
                    for another_result in more_results:
                        all_values.append(another_result)
    return all_values

def get_all_values_level(dictionary):
    all_values=[]
    for key, value in dictionary.iteritems():
        all_values.append(key)

        if isinstance(value, dict):
            results = get_all_values_level(value)
            #for result in results:
            all_values.append(results)

        elif isinstance(value, list):
            for item in value:
                if isinstance(item, dict):
                    more_results = get_all_values_level(item)
                    #for another_result in more_results:
                    all_values.append(more_results)
    return all_values


def get_key_recursively(search_dict, keys):
    """
    Takes a dict with nested lists and dicts,
    and searches all dicts for a key of the keys
    provided.
    """
    keys_found = []

    for key, value in search_dict.iteritems():

        #if key == keys:
        if str(keys) in str(key):
            keys_found.append((key,value))

        elif isinstance(value, dict):
            results = get_key_recursively(value, keys)
            for result in results:
                keys_found.append(result)

        elif isinstance(value, list):
            for item in value:
                if isinstance(item, dict):
                    more_results = get_key_recursively(item, keys)
                    for another_result in more_results:
                        keys_found.append(another_result)

    return keys_found

@cache.cached(timeout=6000)
def request_abr_info(name, limit=5):
    postcode = ''
    legalName = ''
    tradingName = ''
    NSW = 'Y'
    SA = 'Y'
    ACT = 'Y'
    VIC = 'Y'
    WA = 'Y'
    NT = 'Y'
    QLD = 'Y'
    TAS = 'Y'
    endpoint = "http://abr.business.gov.au/abrxmlsearchRPC/AbrXmlSearch.asmx/ABRSearchByNameSimpleProtocol"
    payload = {"name":name,"postcode":postcode,"legalName":legalName,"tradingName":tradingName,"NSW":NSW,"SA":SA,"ACT":ACT,"VIC":VIC,"WA":WA,
            "NT":NT,"QLD":QLD,"TAS":TAS,"authenticationGuid":authenticationGuid}


    r = requests.get(endpoint, params=payload, timeout=30)

    d = xmltodict.parse(r.text)

    results=[]
    if str(u'searchResultsList') not in d[u'ABRPayloadSearchResults'][u'response'].keys():
      return json.dumps(results)

    number_records=int(d[u'ABRPayloadSearchResults'][u'response'][u'searchResultsList'][u'numberOfRecords'])
    number_records = limit if number_records > limit else number_records
    record = d[u'ABRPayloadSearchResults'][u'response'][u'searchResultsList'][u'searchResultsRecord']
    if not isinstance(record,list):
        if str(u'mainTradingName') in record.keys():
             results.append({"name":str(record[u'mainTradingName'][u'organisationName']), "status":str(record[u'ABN'][u'identifierStatus'])})
        if str(u'legalName') in record.keys():
             results.append({"name":str(record[u'legalName'][u'fullName']), "status":str(record[u'ABN'][u'identifierStatus'])})
    if isinstance(record,list):
        for i in range(number_records):
            if str(u'mainName') in record[i].keys():
                results.append({"name":str(record[i][u'mainName'][u'organisationName']), "status":str(record[i][u'ABN'][u'identifierStatus'])})
            if str(u'mainTradingName') in record[i].keys():
                results.append({"name":str(record[i][u'mainTradingName'][u'organisationName']), "status":str(record[i][u'ABN'][u'identifierStatus'])})
            elif str(u'legalName') in record[i].keys():
                results.append({"name":str(record[i][u'legalName'][u'fullName']), "status": str(record[i][u'ABN'][u'identifierStatus'])})

    return json.dumps(results)

@cache.memoize(timeout=6000)
def get_ipgod_results(name):
  endpoint = "http://data.gov.au/api/action/datastore_search"
  payload = {"resource_id":"367fbf30-6b73-4d00-a155-5a1dbe51ff25",
          "q": name, "limit": 10}
  r = requests.get(endpoint, params=payload)
  records = json.loads(r.text)["result"]["records"]
  results_list = []
  result_keys = set()
  for i in range(len(records)):
    tm_number = str(records[i]['tm_number'])
    trademark = str(records[i]['trademark'])
    # Now search IPGOD 201 for the status of the trademark
    endpoint_for_status = 'http://data.gov.au/api/action/datastore_search_sql?sql=SELECT%20*%20from%20%221977143a-5a0c-490a-859e-9a2d8f375d25%22%20WHERE%20tm_number%20=%20%27' + tm_number + '%27'
    status_details = requests.get(endpoint_for_status, timeout=30)
    if len(json.loads(status_details.text)["result"]["records"]) > 0:
        status = str(json.loads(status_details.text)["result"]["records"][0]['cpi_status'])
    else:
        status = 'Unknown'
    # Search IPGOD 204 to get the trademark description and class details
    endpoint_for_class = 'http://data.gov.au/api/action/datastore_search_sql?sql=SELECT%20*%20from%20%2299182f54-e79a-45cc-945f-97fe4e0936c2%22%20WHERE%20tm_number%20=%20%27' + tm_number + '%27'
    class_details = requests.get(endpoint_for_class, timeout=30)
    if len(json.loads(class_details.text)["result"]["records"]) > 0:
      class_obj = str(json.loads(class_details.text)["result"]["records"][0]['class_code'])
      description_text = str(json.loads(class_details.text)["result"]["records"][0]['description_text'])
    else:
      class_obj = 'Unknown'
      description_text = ''
    key = trademark + status + class_obj
    if (key not in result_keys):
      results_list.append([trademark, status, class_obj, description_text])
      result_keys.add(key)
  return results_list

def is_int(s):
  try:
    int(s)
    return True
  except ValueError:
    return False

def _category_sort_key(category_sort_order, result):
  if is_int(result[2]) == False:
    return len(category_sort_order)
  else:
    return category_sort_order[int(result[2]) - 1]

def _status_sort_key(result):
  # Default status sort order
  status_sort_order = {'Registered': 0, 'Pending': 1, 'Removed': 2, 'Other': 3}
  try:
    out = status_sort_order[result[1]]
  except KeyError:
    out = len(status_sort_order)
  return out


@cache.memoize(timeout=6000)
def request_ipgod_info(name, description=None, limit=5):
    # First search IPGOD 222 for the name of the trademark
    results_list = get_ipgod_results(name)
    # Now sort the results by status and category (determined by description)
    sorted_list = []
    category_sort_order_map = list(range(1, 46))
    if description != None:
      category_sort_order = tc.trademark_sort(description)
      # Build a reverse map for easy sorting.
      for k,v in enumerate(category_sort_order):
        category_sort_order_map[v-1] = k
    # First sort by status
    results_list = sorted(results_list, key = lambda result: _status_sort_key(result))
    # Now sort by category according to the new sort order
    sorted_list = sorted(results_list, key = lambda result: _category_sort_key(category_sort_order_map, result))
    # Format the output into JSON
    out_record = []
    for result in sorted_list[:limit]:
        out_record.append({"trademark":result[0], "status": result[1], "class": result[2], "description": result[3]})
    return json.dumps(out_record)

@cache.cached(timeout=6000)
def get_url_info(name):
  prefix = [""]
  postfix = [".com", ".net", ".com.au", ".net.au", ".io"]
  combinations = ((x, y) for x in prefix for y in postfix)
  endpoint = "http://dig.jsondns.org/IN/"
  resource = "/NS"
  status = "unknown"
  output_records = []
  for (pre, post) in combinations:
    site = pre + name + post
    site = ''.join(site.split())
    try:
      response = requests.get(endpoint + site + resource, timeout=3)
      response = json.loads(response.text)
      if (response["header"]["ancount"] == 0):
        status = "available"
      elif (response["header"]["ancount"] > 0):
        status = "unavailable"
      output_records.append({"name":site, "status":status})
    except:
      output_records.append({"name":site, "status":"unknown"})
      next
      continue
  return json.dumps(output_records)


@app.route('/ipgod/<name>')
def get_ip_info(name):
  desc = flask_request.args.get('desc')
  return request_ipgod_info(name, desc)

@app.route('/abr/<name>')
def get_abr_info(name):
    return request_abr_info(name)

@app.route('/url_check/<name>')
def url_checker(name):
    return get_url_info(name)

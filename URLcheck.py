from __future__ import print_function
import json
import requests
import sys

def get_url_info(name):
  prefix = [""]
  postfix = [".com", ".net", ".com.au", ".net.au", ".io"]
  combinations = ((x, y) for x in prefix for y in postfix)
  endpoint = "http://dig.jsondns.org/IN/"
  resource = "/NS"
  status = "unknown"
  output_records = []
  for (pre, post) in combinations:
    site = pre + name + post
    site = ''.join(site.split())
    print (endpoint + site + resource)
    try:
      response = requests.get(endpoint + site + resource, timeout=3)
      response = json.loads(response.text)
      if (response["header"]["ancount"] == 0):
        status = "available"
      elif (response["header"]["ancount"] > 0):
        status = "unavailable"
      output_records.append({"name":site, "status":status})
    except:
      output_records.append({"name":site, "status":"unknown"})
      next
      continue
  return json.dumps(output_records)

if "__main__"==__name__:
  print (get_url_info(sys.argv[1]))

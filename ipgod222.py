from __future__ import print_function
import requests
import json
from trademarklearner import trademarkClassifier

def request_ipgod_info(name, description, limit=5):
    # First search IPGOD 222 for the name of the trademark
    endpoint = "http://data.gov.au/api/action/datastore_search"
    payload = {"resource_id":"367fbf30-6b73-4d00-a155-5a1dbe51ff25",
            "q": name, "limit": limit}
    r = requests.get(endpoint, params=payload) 
    records = json.loads(r.text)["result"]["records"]
    results_list = []
    for i in range(len(records)):
        tm_number = str(records[i]['tm_number'])
        trademark = str(records[i]['trademark'])

        # Now search IPGOD 201 for the status of the trademark
        endpoint_for_status = 'http://data.gov.au/api/action/datastore_search_sql?sql=SELECT%20*%20from%20%221977143a-5a0c-490a-859e-9a2d8f375d25%22%20WHERE%20tm_number%20=%20%27' + tm_number + '%27'
        status_details = requests.get(endpoint_for_status)
        if len(json.loads(status_details.text)["result"]["records"]) > 0:
            status = str(json.loads(status_details.text)["result"]["records"][0]['cpi_status'])
        else:
            status = 'Unknown'

        # Search IPGOD 204 to get the trademark description and class details
        endpoint_for_class = 'http://data.gov.au/api/action/datastore_search_sql?sql=SELECT%20*%20from%20%2299182f54-e79a-45cc-945f-97fe4e0936c2%22%20WHERE%20tm_number%20=%20%27' + tm_number + '%27'
        class_details = requests.get(endpoint_for_class)
        if len(json.loads(class_details.text)["result"]["records"]) > 0:
            class_obj = str(json.loads(class_details.text)["result"]["records"][0]['class_code'])
            description_text = str(json.loads(class_details.text)["result"]["records"][0]['description_text'])
        else:
            class_obj = 'Unknown'
            description_text = ''
        results_list.append([trademark, status, class_obj, description_text])
    print('Search complete')

    sorted_list = []
    if description == '':
        category_sort_order = range(1, 46)
    else:
        category_sort_order = trademarkClassifier.trademark_sort(description)
    # Default status sort order
    status_sort_order = ['Registered', 'Pending', 'Other']
    for status in status_sort_order:
        for category in category_sort_order:
            for result in results_list:
                if (result[1] == status and result[2] == str(category)) or (status == 'Other' and result[2] == str(category)):
                    sorted_list.append(result)

    out_record = []
    for result in sorted_list:
        out_record.append({"trademark": result[0], "status": result[1], "class": result[2], "description": result[3]})
    return out_record


if __name__ == "__main__":
    print(request_ipgod_info("nike", "shoes"))

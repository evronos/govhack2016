# BizNamr service
Development branch for the biznamr app.Based on the angular 2 quickstart example.

## Installation

### Pre installation

This repository makes use of the web-technologies nodejs, angularjs 2 (TypeScript) and Python FLASK for REST Endpoints.
    1. Install node>=5.0 and npm>=2.6.0

Assuming you have pip installed (if not use your system package manager to install pip) you can run,

    2. sudo pip install flask
    3. sudo pip install flask_cors
    4. sudo pip install requests
    5. sudo pip install xmltodict

### Installation


1. Clone the repository from bitbucket url.
#### Client server
    1. cd quickstart
    2. npm install
    3. (potentially) If something goes wrong below, all dependencies may not have been installed properly. The most likely culprit is typescript,
        npm install -g typescript
    4. npm start

#### Flask Server
We use a flask server to provide an interface between our endpoint and the APIS we connect to. On a live production system it will be accessed by a reverse proxy, here it is sitting on your localhost.

In order to run the server you need an authentication token to the ABR api. For the purposes of GovHack it is simply GovHack2016. Place a text file with just this text in a new folder in server/private/abr_authentication_token.txt. The server reads the key from this file.
    
    1. cd server
    2. export FLASK_APP=server.py
    3. flask run
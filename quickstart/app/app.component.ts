import {Component, OnInit, ChangeDetectionStrategy} from '@angular/core';
import {Http, Response, Request, Headers, RequestOptions, RequestMethod, URLSearchParams} from '@angular/http'
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Component({
    selector: 'businesshelp',
    template: `
    <form class="form-wrapper cf">
        <div>
            <input type="text" id="name" class="form-control"
            [(ngModel)]="name" name="name" placeholder="Enter a potential business name here" required/>
            <input type="text" id="name" class="form-control"
            [(ngModel)]="desc" name="desc" placeholder="(optional) describe your business" />
            <button type="submit"  (click)="onClick()" (keyup.enter)="onClick()">Search</button>
        </div>
    </form>

    <div [ngClass]="{'w3-row-padding':true, 'w3-center':true, 'w3-margin-top':true }">
    <div [ngClass]="{'hide_class': is_visible, 'w3-third': true}">
        <div class="w3-card-2 w3-padding-top" style="min-height:460px">
        <header class="w3-container">
            <h2>Australian Business Register</h2>
        </header>

        <div class="w3-container">
        <ul class= "w3-ul">
        <li style="list-style-type:none;" *ngFor="let item of request_abr;">
            Found business <span class="item">{{item.name}}</span><br />
            with status: <span class="item">{{item.status}}</span>.
        </li>
        <li [ngClass]="{'no-sessions':true, 'loaded':abr_loaded}" > The business <b>{{ name }}</b> is potentially available. </li>
        </ul>
        </div>

    </div>
    </div>
    <div [ngClass]="{'hide_class': is_visible, 'w3-third': true}">
      <div class="w3-card-2 w3-padding-top" style="min-height:460px">
        <header class="w3-container">
            <h2>Domain name</h2>
        </header>
    <div class="w3-container">
    <ul class= "w3-ul">
        <li style="list-style-type:none;" *ngFor="let item of request_url;">
        <a href="https://www.{{item.name}}">www.{{item.name}}</a> is {{item.status}}.
        </li>
    <li [ngClass]="{'no-sessions':true, 'loaded':url_loaded}" > Domains related to <b>{{ name }}</b> are potentially all available. </li>
    </ul>
    </div>

    </div>
    </div>
    <div [ngClass]="{'hide_class': is_visible, 'w3-third': true}">
        <div class="w3-card-2 w3-padding-top" style="min-height:460px">
        <header class="w3-container">
            <h2>Australian Trademarks</h2>
        </header>
        <div class="w3-container">
        <ul class= "w3-ul">
        <li style="list-style-type:none;" *ngFor="let item of request_ipgod;">
            Found trademark <span class="item">{{item.trademark}}</span> with <br />
            status <span class="item">{{item.status}}</span> in <br />
            trademark class <span class="item">{{item.class}}</span>.
        </li>
        <li [ngClass]="{'no-sessions':true, 'loaded':ipgod_loaded}" > The trademark <b>{{ name }}</b> is potentially available. </li>
        </ul>
        </div>
        </div>
    </div>
    </div>
    <div class="w3-container w3-padding-32 w3-center w3-opacity w3-margin-bottom">
    <footer [ngClass]="{'hide_class': is_visible}">
        <p>*This search is only a guide, and we recommend talking to an expert to verify our results.</p>
    </footer>
    </div>
`

})
export class AppComponent implements OnInit {
  name: string;
  is_visible : boolean;
  desc : string;

  public request_ipgod : Object;
  public request_abr: Object;
  public request_url: Object;

  public ipgod_loaded : boolean;
  public abr_loaded : boolean;
  public url_loaded : boolean;

  http: Http;

  constructor(http: Http) {
    this.is_visible = true;
    this.name = "";
    this.desc = "";
    this.http = http;
  }
  onClick()
  {
    this.ipgod_loaded = false;
    this.abr_loaded = false;
    this.url_loaded = false;

    this.request_ipgod = [];
    this.request_abr = [];
    this.request_url = [];

    var url: string = "http://localhost:5000";
    var endpoint : string = url + "/ipgod/";
    var query : string = endpoint + this.name;
    if (this.desc != "")
    {
      query = query + "?desc=" + this.desc;
    }
    this.http.get(query)
          .map((res: Response) => res.json())
          .subscribe(res => {this.request_ipgod = res; this.ipgod_loaded = true;});

    endpoint = url + "/abr/";
    this.http.get(endpoint + this.name)
            .map((res: Response) => res.json())
            .subscribe(res => {this.request_abr = res; this.abr_loaded = true;});

    endpoint = url + "/url_check/";
    this.http.get(endpoint + this.name)
            .map((res: Response) => res.json())
            .subscribe(res => {this.request_url = res; this.url_loaded = true;});
    this.is_visible = false;
  }

  watcher()
  {

  }
  ngOnInit()
  {
  }
}

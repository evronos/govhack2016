from __future__ import print_function
import requests
import json
import xmltodict
import sys
from lxml import objectify
from json import dumps

def get_all_values(dictionary):
    all_values=[]
    for key, value in dictionary.iteritems():
        all_values.append(key)

        if isinstance(value, dict):
            results = get_all_values(value)
            for result in results:
                all_values.append(result)

        elif isinstance(value, list):
            for item in value:
                if isinstance(item, dict):
                    more_results = get_all_values(item)
                    for another_result in more_results:
                        all_values.append(another_result)
    return all_values

def get_all_values_level(dictionary):
    all_values=[]
    for key, value in dictionary.iteritems():
        all_values.append(key)

        if isinstance(value, dict):
            results = get_all_values_level(value)
            #for result in results:
            all_values.append(results)

        elif isinstance(value, list):
            for item in value:
                if isinstance(item, dict):
                    more_results = get_all_values_level(item)
                    #for another_result in more_results:
                    all_values.append(more_results)
    return all_values


def get_key_recursively(search_dict, keys):
    """
    Takes a dict with nested lists and dicts,
    and searches all dicts for a key of the keys
    provided.
    """
    keys_found = []

    for key, value in search_dict.iteritems():

        #if key == keys:
        if str(keys) in str(key):
            keys_found.append((key,value))

        elif isinstance(value, dict):
            results = get_key_recursively(value, keys)
            for result in results:
                keys_found.append(result)

        elif isinstance(value, list):
            for item in value:
                if isinstance(item, dict):
                    more_results = get_key_recursively(item, keys)
                    for another_result in more_results:
                        keys_found.append(another_result)

    return keys_found

def request_abr_info(name):
    postcode = ''
    legalName = ''
    tradingName = ''
    NSW = 'Y'
    SA = 'Y'
    ACT = 'Y'
    VIC = 'Y'
    WA = 'Y'
    NT = 'Y'
    QLD = 'Y'
    TAS = 'Y'
    authenticationGuid = 'GovHack2016'
    endpoint = "http://abr.business.gov.au/abrxmlsearchRPC/AbrXmlSearch.asmx/ABRSearchByNameSimpleProtocol"
    payload = {"name":name,"postcode":postcode,"legalName":legalName,"tradingName":tradingName,"NSW":NSW,"SA":SA,"ACT":ACT,"VIC":VIC,"WA":WA,
            "NT":NT,"QLD":QLD,"TAS":TAS,"authenticationGuid":authenticationGuid}


    r = requests.get(endpoint, params=payload)

    d = xmltodict.parse(r.text)

    #print('keyname:', get_all_values_level(d)[-1][-1][-1])
    #print('keycontent:',get_key_recursively(d,'Name'))
    #print('numberofrecords:',get_key_recursively(d,'numberOfRecords'))
    #print (d.keys())
    #print (d[u'ABRPayloadSearchResults'].keys())
    #print (d[u'ABRPayloadSearchResults'][u'request'][u'nameSearchRequest'])
    #print (d[u'ABRPayloadSearchResults'][u'response'][u'searchResultsList'].keys())
    #print (d[u'ABRPayloadSearchResults'][u'response'][u'searchResultsList'][u'searchResultsRecord'])
    #print (d[u'ABRPayloadSearchResults'][u'response'][u'searchResultsList'][u'searchResultsRecord'].keys())
    #print (d[u'ABRPayloadSearchResults'][u'response'][u'searchResultsList'][u'searchResultsRecord'][0])
    #print (d[u'ABRPayloadSearchResults'][u'response'][u'searchResultsList'][u'searchResultsRecord'][u'mainTradingName'].keys())
    #print (d[u'ABRPayloadSearchResults'][u'response'][u'searchResultsList'][u'searchResultsRecord'][0][u'mainName'].keys())
    #print (d[u'ABRPayloadSearchResults'][u'response'][u'searchResultsList'][u'searchResultsRecord'][0][u'legalName'].keys())
    #print (d[u'ABRPayloadSearchResults'][u'response'][u'searchResultsList'][u'searchResultsRecord'][0].keys())
    #print (str(d[u'ABRPayloadSearchResults'][u'response'][u'searchResultsList'][u'searchResultsRecord'][0][u'mainBusinessPhysicalAddress'].keys()))
    #print (d[u'ABRPayloadSearchResults'][u'response'][u'searchResultsList'][u'searchResultsRecord'][0][u'ABN'].keys())


    if str(u'searchResultsList') not in d[u'ABRPayloadSearchResults'][u'response'].keys():
        return {}
    number_records=int(d[u'ABRPayloadSearchResults'][u'response'][u'searchResultsList'][u'numberOfRecords'])
    results=[]
    record = d[u'ABRPayloadSearchResults'][u'response'][u'searchResultsList'][u'searchResultsRecord']
    #for i in range(number_records):
    if not isinstance(record,list):
        if str(u'mainTradingName') in record.keys():
             results.append({"name":str(record[u'mainTradingName'][u'organisationName']), "status":str(record[u'ABN'][u'identifierStatus'])})
        if str(u'legalName') in record.keys():
             results.append({"name":str(record[u'legalName'][u'fullName']), "status":str(record[u'ABN'][u'identifierStatus'])})
    if isinstance(record,list):
        for i in range(number_records):
            if str(u'mainName') in record[i].keys():
                results.append({"name":str(record[i][u'mainName'][u'organisationName']), "status":str(record[i][u'ABN'][u'identifierStatus'])})
            if str(u'mainTradingName') in record[i].keys():
                results.append({"name":str(record[i][u'mainTradingName'][u'organisationName']), "status":str(record[i][u'ABN'][u'identifierStatus'])})
            elif str(u'legalName') in record[i].keys():
                results.append({"name":str(record[i][u'legalName'][u'fullName']), "status": str(record[i][u'ABN'][u'identifierStatus'])})

    return json.dumps(results)


if __name__=="__main__":
    print (request_abr_info("evronos"))
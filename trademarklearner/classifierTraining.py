import csv
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
import pickle

from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.optimizers import Adam
from keras import callbacks

ipGodDirectory = '../ipgod204-04022016.csv'

np.random.seed(1337)  # for reproducibility
batch_size = 500
nb_epoch = 50
dropout = 0.5
data_rows = 100000   # Total rows are 2428442
tm_categories = 45

data_file = open(ipGodDirectory)
reader = csv.reader(data_file)
# Load the data, getting rid of the header row
raw_data = list(reader)[1:data_rows]

# corpus is a list, where each entry is a trademark description
corpus = []
# Here is the target matrix. There are 45 trademark categories.
Y = np.zeros((data_rows-1, 45))

for i in range(len(raw_data)):
    corpus.append(raw_data[i][3])
    Y[i, int(raw_data[i][1])] = 1

vectorizer = CountVectorizer(min_df=1, max_df=0.5, lowercase=True)
X = vectorizer.fit_transform(corpus).toarray()

# We save the vectorizer, so that input sentences can be converted to input vectors
with open('vectorizer', 'wb') as f:
    pickle.dump(vectorizer, f)


# Build the model
model = Sequential()
model.add(Dense(200, input_shape=(len(X[0,:]),)))
model.add(Activation('relu'))
model.add(Dropout(dropout))
model.add(Dense(150))
model.add(Activation('relu'))
model.add(Dropout(dropout))
model.add(Dense(100))
model.add(Activation('relu'))
model.add(Dropout(dropout))
model.add(Dense(tm_categories))
model.add(Activation('softmax'))

model.summary()

model.compile(loss='categorical_crossentropy', optimizer=Adam(), metrics=['accuracy'])
early_stopping = callbacks.EarlyStopping(monitor='val_loss', patience=10)
hist = model.fit(X, Y, batch_size=batch_size,
                 nb_epoch=nb_epoch, validation_split=0.1, callbacks=[early_stopping])

weightsList = model.get_weights()

with open('weights', 'wb') as f:
    pickle.dump(weightsList, f)






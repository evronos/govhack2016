import numpy as np
import pickle
import csv


def _relu(x):
    x[x<0] = 0
    return x

def _softmax(x):
  x = (x.T - np.max(x, axis=1)).T
  expons = np.exp(x)
  denoms = np.sum(expons, axis=1)
  x = expons * (1.0 / denoms)
  return x

class TrademarkClassifier:

  def __init__(self):
    # Load the data
    with open('data/vectorizer', 'rb') as f:
        self.vectorizer = pickle.load(f)
    with open('data/weights', 'rb') as f:
        self.weights_list = pickle.load(f)

  # Get input trademark description
  def trademark_sort(self, description):
    input_vector = self.vectorizer.transform([description]).toarray()
    a1 = _relu(input_vector.dot(self.weights_list[0]) + self.weights_list[1])
    a2 = _relu(a1.dot(self.weights_list[2]) + self.weights_list[3])
    a3 = _relu(a2.dot(self.weights_list[4]) + self.weights_list[5])
    a4 = _relu(a3.dot(self.weights_list[6]) + self.weights_list[7])
    output = _softmax(a4)
    order = list(np.argsort(output)[0] + 1)
    order.reverse()
    return order

if __name__ == "__main__":
  tc = TrademarkClassifier()
  description = 'Chemical fertiliser'
  order = tc.trademark_sort(description)
  print(order)
